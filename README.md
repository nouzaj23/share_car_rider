# PV168 Project: Share Car Rider

Desktop application which allows the user to track his personal rides (eg. regular rides, trips...).
It gives an overview of how much he spends on fuel, how many passengers he's had and total distance travelled.


## Team Information

| Seminar Group | Team |
|-------------- | ---- |
| PV168/01      | 2    |

### Members

| Role           | Person               |
|----------------|----------------------|
|Team Lead       | [Martin Tuček](https://is.muni.cz/auth/osoba/524838) |
|Member          | [Jan Nouza](https://is.muni.cz/auth/osoba/524780) |
|Member          | [Jonáš Jadrníček](https://is.muni.cz/auth/osoba/514017) |
|Member          | [Adam Paulen](https://is.muni.cz/auth/osoba/525093) |

### Evaluators

| Role           | Person               |
|----------------|----------------------|
|PM              | [Šimon Mačejovský](https://is.muni.cz/auth/osoba/444140) |
|Technical Coach | [Peter Balčirák](https://is.muni.cz/auth/osoba/422570) |


## Screenshots

[![Screenshot-2024-04-06-at-14-15-42.png](https://i.postimg.cc/Qd9gb0Xx/Screenshot-2024-04-06-at-14-15-42.png)](https://postimg.cc/gxp6cyXf)

[![Screenshot-2024-04-06-at-14-15-55.png](https://i.postimg.cc/ZRZrJm6C/Screenshot-2024-04-06-at-14-15-55.png)](https://postimg.cc/hJyJ0HyB)

[![Screenshot-2024-04-06-at-14-16-26.png](https://i.postimg.cc/q72sfqhP/Screenshot-2024-04-06-at-14-16-26.png)](https://postimg.cc/VSLrnYFg)

[![Screenshot-2024-04-06-at-14-16-47.png](https://i.postimg.cc/wjCXbLv5/Screenshot-2024-04-06-at-14-16-47.png)](https://postimg.cc/fVvSV3Yk)
